package hotel.entities;

import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;

@ExtendWith(MockitoExtension.class)
class TestRoom {
	@Mock Booking booking;
	@Spy ArrayList<Booking>bookings;
	
	int roomId = 1;
	
	RoomType roomType = RoomType.SINGLE;
	@InjectMocks Room room = new Room(roomId, roomType);

	@BeforeEach
	public void setUp() throws Exception {
	
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckinWhenReady() {
		//arrange
		//act
		Room.checkin();
		//assert
		Assert.assertTrue(Room.isOccupied());
	}
	
	@Test
	public void testCheckinWhenOccupied() {

		Room.checkin();
		Assert.assertTrue(Room.isOccupied());
		Executable e = () ->Room.checkin();
		Throwable t = assertThrows(RuntimeException.class,e);
		Assert.assertEquals("Cannot checkin a room that is not Ready",t.getMessage());
			}
	private Throwable assertThrows(Class<RuntimeException> class1, Executable e) {
		// TODO Auto-generated method stub
		return null;
	}

	@Test
	public void testCheckoutWhenOccupied() {

		bookings.add(booking);
		Room.checkin();
		Assert.assertEquals(1,bookings.size());
		Assert.assertTrue(Room.isOccupied());
		room.checkout(booking);
		
		verify(bookings).remove(booking);
		Assert.assertTrue(room.isReady());
		Assert.assertEquals(0,bookings.size());
			}

	private ArrayList<Booking> verify(ArrayList<Booking> bookings2) {
		// TODO Auto-generated method stub
		return null;
	}
}

